var passport = require('passport');
var mongoose = require('mongoose');
require('../models/token');
var Token = mongoose.model('token');
var nodemailer = require("nodemailer");
var crypto = require('crypto');
require('../models/user.js');
var User = mongoose.model('user');

var JSONResponse = function(res, status, data) {
  res.status(status);
  res.json(data);
};


module.exports.change = function(req, res) {
  //pword, rpword, npword, uname
  if (!req.body.pword || !req.body.rpword || !req.body.npword || !req.body.uname) {
    JSONResponse(res, 400, {
      "message": "Missing something"
    });
    return false;
  } else if(!/\d/.test(req.body.npword) || !/[A-Z]/.test(req.body.npword) || !/[a-z]/.test(req.body.npword) || !req.body.npword.length > 7){
    JSONResponse(res, 400, {
      "message": "Password doesn't meet the requirements."
    });
    return false;   
  }
  User.findOne({userName: req.body.uname},
      function(err, user) {
        if (err)
          return JSONResponse(res, 400, err);
        if (!user) {
          return JSONResponse(res, 400, err);
        }
        if (!user.checkPwd(req.body.pword)) {
          return JSONResponse(res, 400, err);
        }
        console.log(user);
        user.makePwrod(req.body.npword);
      
        user.save(function(error, content){
            if(error){
                JSONResponse(res, 400, error);
            }else{
                JSONResponse(res, 200, {
                  "message": "Successfully changed."
                });
                
            }
        });
      }
  );
};

module.exports.registration = function(req, res) {
  var defStatus = 1;
  if (!req.body.uname || !req.body.pword) {
    JSONResponse(res, 400, {
      "message": "Missing something"
    });
  } else if(!/\d/.test(req.body.pword) || !/[A-Z]/.test(req.body.pword) || !/[a-z]/.test(req.body.pword) || !req.body.pword.pword > 7){
    JSONResponse(res, 400, {
      "message": "Password doesn't meet the requirements."
    });
    return false;   
  }
  var curUser = new User();
  curUser.userName = req.body.uname;
  if(req.body.status)
    curUser.status = req.body.status;
  else
    curUser.status = defStatus;
  curUser.confirmationStatus = 0;
  curUser.makePwrod(req.body.pword);
  curUser.save(function(err) {
   if (err) {
     if(err.code == 11000){
       JSONResponse(res, 400, {
         "message": "User with that username allready exists."});
     }else{
       JSONResponse(res, 500, err);
     }
   } else {
     sendConfMail(req, res, curUser);
     JSONResponse(res, 200, {
       "message": "We've sent you a email please follow the link that we've provided"
     });
   }
  });
};


module.exports.login = function(req, res) {
  if (!req.body.uname || !req.body.pword) {
    JSONResponse(res, 400, {
      "message": "Missing something"
    });
    return;
  }
  
  passport.authenticate('local', function(err, user, data) {
    if (err) {
      JSONResponse(res, 404, err);
      return;
    }
    else if (user) {
      JSONResponse(res, 200, {
        "token": user.generateJWT()
      });
    } else {
      JSONResponse(res, 401, data);
    }
  })(req, res);
};

function sendConfMail(req, res, curUser) {
  var token = new Token({ _userId: curUser._id, token: crypto.randomBytes(16).toString('hex') });
  token.save(function (err) {
    if(err){
      console.log(err);
      return;
    }else{
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "skupina17.tpo@gmail.com",
          pass: "Randomgeslo1."
        }
      });
    
      // Message object
      console.log("Username: " + curUser.userName);
      let message = {
          from: 'skupina17.tpo@gmail.com',
          // Comma separated list of recipients
          to: curUser.userName,
    
          // Subject of the message
          subject: 'Please confirm your email',
    
          // plaintext body
          text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/confirmation\/' + token.token + '.\n'
    
      };
    
      transporter.sendMail(message, (error, info) => {
          if (error) {
              console.log('Error occurred');
              console.log(error.message);
              return process.exit(1);
          }
    
          console.log('Message sent successfully!');
          console.log(nodemailer.getTestMessageUrl(info));
    
          // only needed when using pooled connections
          transporter.close();
      });
    }
  });
}

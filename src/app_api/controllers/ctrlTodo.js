var mongoose = require('mongoose');
require('../models/todo.js');
var todo = mongoose.model('todo');

var JSONResponse = function(res, status, content){
    res.status(status);
    res.json(content);
};

module.exports.entriesGetAll = function(req, res){
    todo.find({})
    .exec(function(error, content){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, content);
        }
    });
};

module.exports.entriesGetAllByBEmail = function(req, res){
    if(!req.body.email){
        JSONResponse(res, 400, {
            message: "You are not logged in."
        });
        return;
    }
    todo.find({email : req.body.email})
    .exec(function(error, content){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, content);
        }
    });
};


module.exports.entriesGetAllByEmail = function(req, res){
    todo.find({email : req.params.email})
    .exec(function(error, content){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, content);
        }
    });
};

module.exports.entriesAdd = function(req, res){
    if(!req.body.email || !req.body.desc || !req.body.title){
        JSONResponse(res, 400, {
            "message": "Missing fields"
        });
        return;
    }

    todo.create({
        email : req.body.email,
        desc : req.body.desc,
        title: req.body.title
    }, 
    function(error, entry){
        if(error){
            JSONResponse(res, 400, error);
        }else{
            JSONResponse(res, 200, entry);
        }
    });
};

module.exports.entriesUpdate = function(req, res){
    console.log(req.params.id);
    todo.findOne({_id: req.params.id})
    .exec(function(error, entry){
        if(!entry){
            JSONResponse(res, 404, "Not found.");
            return;
        }else{
            if(req.body.email){
                entry.email = req.body.email;
            }
            if(req.body.desc){
                entry.desc = req.body.desc;
            }
            if(req.body.title)
                entry.title = req.body.title;
            entry.save(function(error, entry){
                if(error){
                    JSONResponse(res, 400, error);
                }else{
                    JSONResponse(res, 200, entry);
                }
            });
        }
    });
};

module.exports.entriesDelete = function(req, res){
    todo.findOneAndDelete( {_id: req.params.id})
    .exec(function(error, entry){
        if(!entry){
            JSONResponse(res, 404, "Not found");
            return;
        }else if(error){
            JSONResponse(res, 500, "There was an error.");
            return;
        }
        JSONResponse(res, 200, entry);
    });
};


module.exports.entriesDeleteByEmail = function(req, res){
    todo.findOneAndDelete( {email: req.params.email})
    .exec(function(error, entry){
        if(!entry){
            JSONResponse(res, 404, "Not found");
            return;
        }else if(error){
            JSONResponse(res, 500, "There was an error.");
            return;
        }
        JSONResponse(res, 200, entry);
    });
};
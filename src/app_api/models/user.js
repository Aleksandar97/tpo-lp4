var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
  userName: {type: String, unique: true, required: true},
  zgoscenaVrednost: String,
  status: {type: Number, required: true},
  randomValue: {type: String, required: true},
  confirmationStatus: Number
});

userSchema.methods.makePwrod = function(pword) {
  this.randomValue = crypto.randomBytes(16).toString('hex');
  this.zgoscenaVrednost = crypto.pbkdf2Sync(pword, this.randomValue, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.checkPwd = function(pword) {
  var zgoscenaVrednost = crypto.pbkdf2Sync(pword, this.randomValue, 1000, 64, 'sha512').toString('hex');
  return this.zgoscenaVrednost == zgoscenaVrednost;
};

userSchema.methods.generateJWT = function() {
  var dataCreated = new Date();
  dataCreated.setDate(dataCreated.getDate() + 7);
  
  return jwt.sign({
    _id: this._id,
    userName: this.userName,
    status: this.status,
    dataCreated: parseInt(dataCreated.getTime() / 1000, 10)
  }, process.env.JWT_PASS);
};

mongoose.model('user', userSchema);
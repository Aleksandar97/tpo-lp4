package selenium.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test {
	
	public static void main(String[] args) {
		
		//Od prva nikogas ne rabotit login, pojke pati run!!
		
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Win10\\Desktop\\fax\\3 godina\\2 semestar\\tpo\\selenium\\geckodriver\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://straightas-tpo17.herokuapp.com/");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        //ok
        //checkLogin(driver, wait, "nikoloskamaja1@gmail.com", "Testi");
        //System.out.println("Login Izjemni 1 OK");
        checkLogin(driver, wait, "nikoloskamaja1@gmail.com", "Testi12.");
        System.out.println("Login OK");
        //ok
        //checkChangePassword(driver, wait, "Testi12.", "Testi12.", "Testi123.");
        //System.out.println("Reset Password OK");
        /*checkChangePassword(driver, wait, "Testi123.", "Testi13.", "Testi12.");
        System.out.println("Reset Password Izjemni 1 OK");
        checkChangePassword(driver, wait, "Testi123.", "Testi123.", "Testi");
        System.out.println("Reset Password Izjemni 2 OK");*/
        //checkEvents(driver, wait);
        //System.out.println("Events OK");
        //ok
        //addTODO(driver, wait);
        //System.out.println("addTODO OK");
        //not ok dinamicen id vo index.html, ostalo ok (menvit vsebina samo za prviot)
        editTODO(driver, wait);
        System.out.println("editTODO OK");
        //ok
        //deleteTODO(driver, wait, "deleteButtonCancel");
        //System.out.println("deleteTODO Canceled OK");
        //ok
        //deleteTODO(driver, wait, "deleteButtonDelete");
        //System.out.println("deleteTODO OK");
        //not ok da se dodajt lokacija
        //checkFood(driver, wait);
        //System.out.println("Food OK");
        
        //da se dodajt datepicker
        //addCalendarEvent(driver, wait);
        //System.out.println("addCalendarEvent OK");
        //ok
        //checkLogout(driver, wait);
        //System.out.println("Logout OK");
	}
	
	public static void checkLogin(WebDriver driver, WebDriverWait wait, String username, String pass) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
        driver.findElement(By.id("login")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("input")));
        List<WebElement> we = driver.findElements(By.tagName("input"));
        we.get(1).sendKeys(username);
        we.get(2).sendKeys(pass);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginButton")));
        driver.findElement(By.id("loginButton")).click();
    }
	
	public static void checkLogout(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("logout")));
        driver.findElement(By.id("logout")).click();
    }
	
	public static void checkChangePassword(WebDriver driver, WebDriverWait wait, String pass, String repeat, String newPass) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("reset")));
        driver.findElement(By.id("reset")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("input")));
        driver.findElement(By.id("currentPassword")).sendKeys(pass);
        driver.findElement(By.id("repeatPassword")).sendKeys(repeat);
        driver.findElement(By.id("newPassword")).sendKeys(newPass);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("resetButton")));
        driver.findElement(By.id("resetButton")).click();
    }
	
	public static void checkEvents(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("events")));
        driver.findElement(By.id("events")).click();
    }
	
	public static void checkFood(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("food")));
        driver.findElement(By.id("food")).click();
    }
	
	public static void addTODO(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addTODO")));
        driver.findElement(By.id("addTODO")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("title")));
        driver.findElement(By.id("title")).click();
        driver.findElement(By.id("titleAddTODO")).sendKeys("Title");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("description")));
        driver.findElement(By.id("description")).click();
        driver.findElement(By.id("descriptionAddTODO")).sendKeys("Description.");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addTODOButtonSave")));
        driver.findElement(By.id("addTODOButtonSave")).click();
    }
	
	public static void addCalendarEvent(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEvent")));
        driver.findElement(By.id("addCalendarEvent")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEventsName")));
        driver.findElement(By.id("addCalendarEventsName")).click();
        driver.findElement(By.id("addCalendarEventName")).sendKeys("Name");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEventsStart")));
        driver.findElement(By.id("addCalendarEventsStart")).click();
        driver.findElement(By.id("addCalendarEventStart")).sendKeys("05-08-2019");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEventsEnd")));
        driver.findElement(By.id("addCalendarEventsEnd")).click();
        driver.findElement(By.id("addCalendarEventEnd")).sendKeys("05-08-2019");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEventSave")));
        driver.findElement(By.id("addCalendarEventSave")).click();
    }
	
	public static void editTODO(WebDriver driver, WebDriverWait wait) {     
		Actions actions = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editTODO")));
        actions.doubleClick(driver.findElement(By.id("editTODO"))).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editTODOsTitle")));
        driver.findElement(By.id("editTODOsTitle")).click();
        driver.findElement(By.id("editTODOTitle")).clear();
        driver.findElement(By.id("editTODOTitle")).sendKeys("Edited title");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editTODOsDescription")));
        driver.findElement(By.id("editTODOsDescription")).click();
        driver.findElement(By.id("editTODODescription")).clear();
        driver.findElement(By.id("editTODODescription")).sendKeys("Edited description.");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editTODOButton")));
        driver.findElement(By.id("editTODOButton")).click();
    }

	public static void deleteTODO(WebDriver driver, WebDriverWait wait, String button) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteTODO")));
        driver.findElement(By.id("deleteTODO")).click();	
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(button)));
        driver.findElement(By.id(button)).click();
    }
	
}

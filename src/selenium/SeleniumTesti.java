package gadgetfreaktesting;
 
import java.util.List;
 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
 
public class SeleniumTest {
   
    public static void main(String[] args) {
       
        /*System.setProperty("webdriver.gecko.driver", "C:\\Users\\Win10\\Desktop\\fax\\3 godina\\2 semestar\\tpo\\selenium\\geckodriver\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://gadget-freak.herokuapp.com/");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        checkLogin(driver, wait);
        System.out.println("Login successful!!");*/
       
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Boda\\eclipse-workspace\\gadgetfreaktesting\\geckodriver\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://straightas-tpo17.herokuapp.com/");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        //checkLogin(driver, wait, "nikoloskamaja1@gmail.com", "Testi");
        //System.out.println("Login Izjemni 1 OK");
       
       
       
        //OBICAN USER
        checkLogin(driver, wait, "nikoloskamaja1@gmail.com", "Testi12.");
        System.out.println("Login OK");
        //checkChangePassword(driver, wait, "Testi12.", "Testi12.", "Testi123.");
        //System.out.println("Reset Password OK");
        /*checkChangePassword(driver, wait, "Testi123.", "Testi13.", "Testi12.");
        System.out.println("Reset Password Izjemni 1 OK");
        checkChangePassword(driver, wait, "Testi123.", "Testi123.", "Testi");
        System.out.println("Reset Password Izjemni 2 OK");*/
        //checkEvents(driver, wait);
        //System.out.println("Events OK");
        addTODO(driver, wait);
        System.out.println("addTODO OK");
        //editTODO(driver, wait);
        //System.out.println("editTODO OK");
        deleteTODO(driver, wait, "deleteButton");
        System.out.println("deleteTODO OK");
        checkBus(driver, wait);
        System.out.println("Bus OK");
   
       
        checkFood(driver, wait);
        System.out.println("Food OK");
        checkEvents(driver, wait);
        System.out.println("Events OK");
        addCalendarEvent(driver, wait);
        System.out.println("Calendar OK");
        checkLogout(driver, wait);
        System.out.println("Logout OK");
       
       
        //ADMIN
        checkLoginAdmin(driver, wait, "ac0576@student.uni-lj.si", "Test1234");
        System.out.println("Admin login OK");
        newNotification(driver, wait);
        System.out.println("New notification OK");
        clearNotification(driver, wait);
        System.out.println("Clear notifications OK");
        checkLogout(driver, wait);
        System.out.println("Admin logout OK");
       
       
        //EVENT MENADZER
        checkLogin(driver, wait, "bogdan037@gmail.com", "Test1234");
        System.out.println("Event manager login OK");
        addEvent(driver, wait);
        System.out.println("Event manager add event OK");
        checkLogout(driver, wait);
        System.out.println("Event manager logout OK");
        System.out.println("ALL OK");
       
       
    }
   
    public static void checkLogin(WebDriver driver, WebDriverWait wait, String username, String pass) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
        driver.findElement(By.id("login")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("input")));
        List<WebElement> we = driver.findElements(By.tagName("input"));
        we.get(1).sendKeys(username);
        we.get(2).sendKeys(pass);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginButton")));
        driver.findElement(By.id("loginButton")).click();
    }
   
    public static void checkLoginAdmin(WebDriver driver, WebDriverWait wait, String username, String pass) {
           wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login")));
            driver.findElement(By.id("login")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("input")));
            List<WebElement> we = driver.findElements(By.tagName("input"));
            we.get(1).sendKeys(username);
            we.get(2).sendKeys(pass);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginButton")));
            driver.findElement(By.id("loginButton")).click();
    }
   
   
   
    public static void checkLogout(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("logout")));
        driver.findElement(By.id("logout")).click();
    }
   
    public static void checkChangePassword(WebDriver driver, WebDriverWait wait, String pass, String repeat, String newPass) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("reset")));
        driver.findElement(By.id("reset")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("input")));
        driver.findElement(By.id("currentPassword")).sendKeys(pass);
        driver.findElement(By.id("repeatPassword")).sendKeys(repeat);
        driver.findElement(By.id("newPassword")).sendKeys(newPass);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("resetButton")));
        driver.findElement(By.id("resetButton")).click();
    }
   
    public static void checkEvents(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("events")));
        driver.findElement(By.id("events")).click();
    }
   
    public static void checkFood(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("food")));
        driver.findElement(By.id("food")).click();
    }
   
    public static void addTODO(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addTODO")));
        driver.findElement(By.id("addTODO")).click();
        driver.findElement(By.id("titleAddTODO")).sendKeys("Title");
        driver.findElement(By.id("descriptionAddTODO")).sendKeys("Description.");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addTODOButtonSave")));
        driver.findElement(By.id("addTODOButtonSave")).click();
    }
   
   
   
    public static void addCalendarEvent(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEvent")));
        driver.findElement(By.id("addCalendarEvent")).click();
        driver.findElement(By.id("addCalendarEventName")).sendKeys("Name");
        driver.findElement(By.id("addCalendarEventStart")).sendKeys("2011-08-19");
        driver.findElement(By.id("addCalendarEventEnd")).sendKeys("2011-08-20");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addCalendarEventSave")));
        driver.findElement(By.id("addCalendarEventSave")).click();
    }
   
    public static void editTODO(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addTODO")));
        driver.findElement(By.id("addTODO")).click();
        driver.findElement(By.id("editTODOTitle")).sendKeys("Edited title");
        driver.findElement(By.id("editTODODescription")).sendKeys("Edited description.");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editTODOButton")));
        driver.findElement(By.id("editTODOButton")).click();
    }
 
    public static void deleteTODO(WebDriver driver, WebDriverWait wait, String button) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home")));
        driver.findElement(By.id("home")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteTODO")));
        driver.findElement(By.id("deleteTODO")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteButtonDelete")));
        driver.findElement(By.id("deleteButtonDelete")).click();    
    }
    public static void checkRegistracion(WebDriver driver, WebDriverWait wait, String username, String password) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("register")));
        driver.findElement(By.id("register")).click();
        final String myemail = username;
        WebElement email = driver.findElement(By.id("Email"));
        email.sendKeys(myemail);
        WebElement password1 = driver.findElement(By.id("Password"));
        password1.sendKeys("password");
        WebElement password2 = driver.findElement(By.id("Repeat Password"));
        password2.sendKeys("password");
        password2.submit();
        // Check the sign up succeeded by checking that the randomized
        // email appears in the website's header bar.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("header-login")));
    }
    public static void newNotification(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("admin-notifications")));
        driver.findElement(By.id("admin-notifications")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("newNotification")));
        driver.findElement(By.id("newNotification")).click();
        driver.findElement(By.id("exampleTextarea")).sendKeys("Test notification..");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Save")));
        driver.findElement(By.id("Save")).click();
    }
    public static void clearNotification(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("admin-notifications")));
        driver.findElement(By.id("admin-notifications")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Clear notification")));
        driver.findElement(By.id("Clear notification")).click();
    }
    public static void checkBus(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("buses")));
        driver.findElement(By.id("buses")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("dugme")));
    }
    public static void addEvent(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("event-manager")));
        driver.findElement(By.id("event-manager")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add button")));
        driver.findElement(By.id("add button")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("nameInput")));
        WebElement input = driver.findElement(By.id("nameInput"));
        input.sendKeys("New event");
        WebElement date = driver.findElement(By.id("example-date-input"));
        date.sendKeys("2011-08-19");
        WebElement organizer = driver.findElement(By.id("orgInput"));
        organizer.sendKeys("Awesome organizer");
        WebElement desc = driver.findElement(By.id("exampleTextarea"));
        desc.sendKeys("Test test 123");
        driver.findElement(By.id("save")).click();
    }
}
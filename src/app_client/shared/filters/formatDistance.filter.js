(function() {

  
  var formatDistance = function() {
        return function(input) {
        
            var array = [];
            for(var objectKey in input) {
                array.push(input[objectKey]);
            }
        
            
            array.sort(function(a, b){
                return  parseFloat(a.dist) - parseFloat(b.dist);
            });
            
            
            return array;
        };
    };
  
  /* global angular */
    angular
    .module('straightas')
    .filter('formatDistance', formatDistance);
})();
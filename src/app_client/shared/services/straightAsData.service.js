(function() {
  var straightAsData = function($http, authentication) {
    
    var loginUser = function(data){
      return $http.post('/api/login/', data);
    };
    
    var regUser = function(data){
      return $http.post('/api/register/', data);
    };
    
    var locationList = function(){
      return $http.get('/api/restaurant/');
    };
    
    var eventList = function(){
      return $http.get('/api/events/');
    };
    
   var eventPost = function(data){
      return $http.post('/api/events/', data);
    };
    
    var todoList = function(data){
      return $http.post('/api/todoget/',data);
    };
    
    var todoPost = function(data){
      return $http.post('/api/todo/', data);
    };
    
    var todoDelete = function(id){
      return $http.delete('/api/todo/' + id);
    };
    
    var editTodo = function(data, id){
      return $http.put('/api/todo/' + id, data);
    };
    var notificationList = function(){
      return $http.get('/api/notifications');
    };
    
    var notificationPost = function(data){
      return $http.post('/api/notifications', data);
    };
    
    var notificationClear = function(){
      return $http.delete('/api/notifications');
    };
    
    var getTT = function(data){
      return $http.post('/api/timetableget', data);
    };
    
    var postTT = function(data){
      return $http.post('/api/timetable', data);
    };
    
    var putTT = function(id, data){
      return $http.put('/api/timetable/' + id, data);
    };
    
    var deleteTT = function(id){
      return $http.delete('/api/timetable/' + id);
    };
    
    var getCalendar  = function(data){
      return $http.post('/api/calendarget', data);
    };
    
    var postCalendar = function(data){
      return $http.post('/api/calendar/', data);
    };
    
    var getBusInfo = function(data, busNum){
      
    console.log("busnum is" + busNum);
    return $http({
    method: 'GET',
     url: 'https://cors-anywhere.herokuapp.com/https://www.trola.si/'+busNum,
    headers: {
        'Accept': 'application/json',
    }
    }).then(data);
    }
    
    return {
      loginUser: loginUser,
      regUser: regUser,
      locationList: locationList,
      eventList : eventList,
      eventPost: eventPost,
      todoList: todoList,
      todoPost: todoPost,
      todoDelete: todoDelete,
      editTodo: editTodo,
      notificationList : notificationList,
      notificationPost : notificationPost,
      notificationClear : notificationClear,
      getTT: getTT,
      postTT: postTT,
      putTT:putTT,
      deleteTT: deleteTT,
      postCalendar: postCalendar,
      getCalendar: getCalendar,
      getBusInfo : getBusInfo
    };
  };
  
  straightAsData.$inject = ['$http', 'authentication'];
  
  /* global angular */
  angular
    .module('straightas')
    .service('straightAsData', straightAsData);
})();
(function() {
  var geolokacija = function() {
    var curLocation = function(data, err, errSup) {
      /* global navigator */
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(data, err);
      } else {
        errSup();
      }
    };
    return {
      curLocation: curLocation
    };
  };
  
  /* global angular */
  angular
    .module('straightas')
    .service('geolocation', geolokacija);
})();
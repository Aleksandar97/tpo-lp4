(function() {
  function deleteModal($uibModalInstance, straightAsData, todoInfo) {
    var vm = this;
    vm.data={};
    vm.userEmail = "";
    
    if(todoInfo.id != undefined && todoInfo.id.length > 0)
      vm.todoId = todoInfo.id;
    
    vm.modalWin = {
      close: function() {
        $uibModalInstance.close();
      },closeS: function(odgovor) {
        $uibModalInstance.close(odgovor);
      }
    };
    
    vm.delteTodo = function(){
      straightAsData.todoDelete(vm.todoId).then(
        function success(res) {
          vm.modalWin.closeS(res.data);
        },
        function error(res) {
          vm.error = res.data.message;
        }  
      );
    };
  }
  
  deleteModal.$inject = ['$uibModalInstance', 'straightAsData', 'todoInfo'];

  /* global angular */
  angular
    .module('straightas')
    .controller('deleteModal', deleteModal);
})();